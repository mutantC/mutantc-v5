// firmware for mutantC v5
// install thes libs before building the firmware
#include "Arduino.h"
#include <Keypad.h>
#include "USB.h"
#include "USBHIDMouse.h"
#include "USBHIDKeyboard.h"
#include <Wire.h>
#include <SparkFun_MAX1704x_Fuel_Gauge_Arduino_Library.h>  // Click here to get the library: http://librarymanager/All#SparkFun_MAX1704x_Fuel_Gauge_Arduino_Library


// First digit mutantC version
// Second digit PCB version
// Third digit Firmware version
String FIRMWARE_VERSION = "5.7.1";

// ESP32-S2 Pin Assignments:
#define HA 8
#define VA 10
#define MOUSE_MID_BUTTON 9
#define LDR 14
#define LCD_BACKLIGHT 13

#define POWER_BUTTON 12
#define POWER_MAIN 11

// I2C battery monitor
SFE_MAX1704X lipo(MAX1704X_MAX17049);  // Create a MAX17049
double voltage = 0;                    // Variable to keep track of LiPo voltage
double batteryPercentage = 0;          // Variable to keep track of LiPo state-of-charge (SOC)
bool alert;                            // Variable to keep track of whether alert has been triggered
bool batteryMonitorDetected = false;   // Don't print battery state if I2C chip is not found

bool LCD_ON = true;
static uint16_t powered_on = 1000;

// For Mouse
int vertZero, horzZero;       // Stores the initial value of each axis, usually around 512
const int sensitivity = 300;  // Higher sensitivity value = slower mouse, should be <= about 500

// For Keypad
const byte KBD_ROWS = 5;
const byte KBD_COLS = 12;
static bool keymapState = 0;
USBHIDMouse mMouse;
USBHIDKeyboard mKeyboard;

// Keypad event codes:
#define KMAP_SWITCH 0x95
#define LCD_TOGG 0x93
#define KMOUSE_RIGHT 0x97
#define KMOUSE_LEFT 0x99
// #define LCD_BRTN_INC 0x94
// #define LCD_BRTN_DEC 0x96


// You can change the keys and add from this list https://github.com/hathach/tinyusb/blob/master/src/class/hid/hid.h#L356-L586
static uint8_t keymapAlpha[] = {
  0x00, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x00,
//      1     2     3     4     5     6     7     8     9     0
  0x2B, 0x14, 0x1A, 0x08, 0x15, 0x17, 0x1C, 0x18, 0x0C, 0x12, 0x13, KMOUSE_RIGHT,
//Tab,  Q     W     E     R     T     Y     U     I     O     P     MouseRight
  0x39, 0x04, 0x16, 0x07, 0x09, 0x0A, 0x0B, 0x0D, 0x0E, 0x0F, 0x2A, 0xE3,
//CL    A     S     D     F     G     H     J     K     L     Bspce Meta
  0xE1, 0x1D, 0x1B, 0x06, 0x19, 0x05, 0x11, 0x10, 0x36, 0x37, 0x28, KMOUSE_LEFT,
//LSh   Z     X     C     V     B     N     M     ,     .     Enter MouseLeft
  0xE0, 0xE2, 0x52, 0x51, 0x50, 0x4F, 0x2C, 0x00, 0x29, 0xE0, KMAP_SWITCH, LCD_TOGG
//LCtl  LAlt  AUp   ADn   AL    AR    SPC         ESC   LCtl  Switch       LCD Toggle
};

static uint8_t keymapSymbols[] = {
  0x00, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F, 0x40, 0x41, 0x42, 0x43, 0x00,
//      F1    F2    F3    F4    F5    F6    F7    F8    F9    F10
  0x2B, 0x44, 0x45, 0x00, 0x00, 0x00, 0x00, 0x35, 0x2F, 0x30, 0x31, KMOUSE_RIGHT,
//Tab   F11   F12   0     0     0     0     `     [     ]     \     MouseRight
  0x39, 0x57, 0x56, 0x55, 0x54, 0x67, 0x00, 0x38, 0x33, 0x34, 0x2A, 0xE3,
//CL    +     -     *     ÷     =     0     /     ;     '     Bspce Meta
  0xE1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x37, 0x28, KMOUSE_LEFT,
//LSh   0     0     0     0     0     0     0     ,     .     Enter MouseLeft
  0xE0, 0xE2, 0x52, 0x51, 0x50, 0x4F, 0x2C, 0x00, 0x29, 0xE0, KMAP_SWITCH, LCD_TOGG
//LCtl  LAlt  AUp   ADn   AL    AR    SPC         ESC   LCtl  Switch       LCD Toggle
};

static char dummyKeypad[KBD_ROWS][KBD_COLS] = {
  { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
  { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 },
  { 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35 },
  { 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 },
  { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59 }
};

byte kbd_row_pins[KBD_ROWS] = { 37, 35, 41, 40, 38 };                       //connect to the row pinouts of the keypad
byte kbd_col_pins[KBD_COLS] = { 34, 36, 42, 39, 1, 2, 3, 6, 7, 5, 4, 33 };  //connect to the column pinouts of the

//initialize an instance of class NewKeypad
Keypad Keyboard = Keypad(makeKeymap(dummyKeypad), kbd_row_pins, kbd_col_pins, KBD_ROWS, KBD_COLS);

static void lcd_backlight_toggle(void) {
  if (LCD_ON) {
    digitalWrite(LCD_BACKLIGHT, LOW);
    LCD_ON = false;
  } else {
    digitalWrite(LCD_BACKLIGHT, HIGH);
    LCD_ON = true;
  }

  Serial.print("lcd_backlight_toggle: ");
  Serial.print(LCD_ON);
  Serial.println();
}

static void switch_Keymap(void) {
  Serial.println("switch_Keymap event");
  if (keymapState) {
    keymapState = 0;
  } else {
    keymapState = 1;
  }
}

static uint8_t mapKey(char key) {
  if (keymapState)
    return keymapSymbols[key];

  return keymapAlpha[key];
}

static void processKeys(void) {
  if (!Keyboard.getKeys())
    return;
  int i;

  for (i = 0; i < LIST_MAX; i++) {
    if (!Keyboard.key[i].stateChanged) {
      continue;
    }

    uint8_t key = mapKey(Keyboard.key[i].kchar);

    Serial.println(key);
    switch (Keyboard.key[i].kstate) {
      case PRESSED:
        {
          Serial.println(key);
          if (key == KMAP_SWITCH) {
            Serial.println("KMAP_SWITCH");
            switch_Keymap();
          } else if (key == KMOUSE_LEFT) {
            Serial.println("KMOUSE_LEFT");
            mMouse.click(MOUSE_LEFT);
          } else if (key == KMOUSE_RIGHT) {
            Serial.println("KMOUSE_RIGHT");
            mMouse.click(MOUSE_RIGHT);
          } else if (key == LCD_TOGG) {
            Serial.println("LCD_TOGG");
            lcd_backlight_toggle();
          } else {
            Serial.println(key);
            mKeyboard.pressRaw(key);
          }
        }
        break;
      case RELEASED:
        {
          mKeyboard.releaseRaw(key);
        }
        break;
    }
  }
}


/*
  reads an axis (0 or 1 for x or y) and scales the analog input range to a range
  from 0 to <range>
*/
static void processMouse() {
  // read and scale the two axes:
  int xReading = analogRead(VA) - vertZero;  // read vertical offset
  int yReading = analogRead(HA) - horzZero;  // read horizontal offset

  if (abs(xReading) > 2 || abs(yReading) > 2) {
    mMouse.move((yReading / sensitivity), (xReading / sensitivity));
  }

  if (!digitalRead(MOUSE_MID_BUTTON)) {
    mMouse.click(MOUSE_MIDDLE);
    Serial.println("pressMiddle");
    delay(20);
  }
}

static void power_off(void) {
  uint8_t i;

  /* Ask RPi to powerdown */
  //mKeyboard.pressRaw(0x66);
  //mKeyboard.releaseRaw(0x66);

  /* Wait for some time */
  for (i = 0; i < 20; i++) {
    Serial.println("power_off_countdown" + i);
  }
  delay(10000);

  /* And finally turn off the power */
  Serial.println("power_off");
  digitalWrite(POWER_MAIN, LOW);
}

static void batteryStats() {
  // lipo.getVoltage() returns a voltage value (e.g. 3.93)
  voltage = lipo.getVoltage();
  // lipo.getSOC() returns the estimated state of charge (e.g. 79%)
  batteryPercentage = lipo.getSOC();
  // lipo.getAlert() returns a 0 or 1 (0=alert not triggered)
  alert = lipo.getAlert();

  // Print the variables:
  Serial.print("Voltage: ");
  Serial.print(voltage);  // Print the battery voltage
  Serial.println(" V");

  Serial.print("Percentage: ");
  Serial.print(batteryPercentage);  // Print the battery state of charge
  Serial.println(" %");

  Serial.print("Alert: ");
  Serial.println(alert);
  Serial.println();

  delay(500);
}

void setup(void) {
  pinMode(POWER_MAIN, OUTPUT);
  digitalWrite(POWER_MAIN, HIGH);

  pinMode(LDR, INPUT);
  pinMode(VA, INPUT);
  pinMode(HA, INPUT);
  pinMode(MOUSE_MID_BUTTON, INPUT_PULLUP);
  pinMode(POWER_BUTTON, INPUT_PULLUP);
  pinMode(LCD_BACKLIGHT, OUTPUT);

  // Serial setup
  Serial.begin(115200);
  Serial.println("Starting verion: " + FIRMWARE_VERSION);
  Serial.println();

  //I2C pins
  Wire.begin(18, 17);

  vertZero = analogRead(VA);  // get the initial values
  horzZero = analogRead(HA);  // Joystick should be in neutral position when reading these

  // analogReadResolution( 13 );
  lcd_backlight_toggle();
  delay(500);
  lcd_backlight_toggle();

  // Delay the USB HID as SBC doesn't recognize HID if started before SBC finished booting
  Serial.println("Wait for SBC to Finish booting, 30 sec");
  delay(30000);
  Serial.println("end");

  // Set up Keyboard and Mouse HID
  mMouse.begin();
  mKeyboard.begin();
  USB.begin();

  // Set up keypad matrix
  Keyboard.setHoldTime(1);
  Keyboard.setDebounceTime(0);
  delay(500);

  // Set up the MAX17049 LiPo fuel gauge:
  if (lipo.begin() == false) {  // Connect to the MAX17049 using the default wire port
    Serial.println(F("MAX17049 not detected"));
    batteryMonitorDetected = false;
  } else {
    Serial.println(F("MAX17049 detected"));
    batteryMonitorDetected = true;

    // Quick start restarts the MAX17043 in hopes of getting a more accurate
    // guess for the SOC.
    lipo.quickStart();

    // We can set an interrupt to alert when the battery percentage gets too low.
    // We can alert at anywhere between 1% - 32%:
    lipo.setThreshold(20);  // Set alert threshold to 20%.
  }
}


void loop(void) {
  // Ignore the power_off button just after it has been pressed to turn the power on
  if (powered_on > 0) {
    powered_on--;
  } else {
    if (!digitalRead(POWER_BUTTON) && digitalRead(POWER_MAIN)) {
      Serial.println("POWER_OFF_BTN");
      power_off();
    }
  }

  //Serial.println("start");

  // Deal with keyboard and mouse interactions
  processKeys();
  processMouse();

  // Show Battery stats
  //if (batteryMonitorDetected) { batteryStats(); }

  //int LDR_m = analogRead(LDR);

  delay(1);  // delay in between reads for stability
}



// written by
// rahmanshaber @rahmanshaber

// Parts of code use example from
// SparkFun
// mutantC v4 firmware
