# IN DEVELOPMENT
# mutantC-v5 - [Project Website](https://mutantcybernetics.gitlab.io/mutantC_V5.html)
<img src="pics/0.JPG" width="500">

## Warning: The source files in this repository may not reflect a stable build and could cause failed build if used as-is. Please use the Build instructions for a working build.

## Features
* Uses Raspberry-pi CM4 form-factor for main SBC.
* 5inch or 4.3inch LCD display with support for another External Display out port.
* 1,2,4,8 GB ram, Wifi, either EMMC or SD card for the storage depending on SBC.
* Uses ESP32-S2 for all the secondary functions including keyboard and mouse control.
* Expansion supports 1x3gen PCIE for many possibilities with add-on cards like SSD, OcuLink.
* Expansion also supports 2 USB2.0 ports, 17 GPIO which includes SPI, I2C, PWM.
* 61 key customizable keyboard, 6 shoulder button for navigation.
* 3D hall-effect joystick is used for proper Mouse control.
* NeoPixel connected to the main SBC for customizable notification.
* 10000mAh(21700) or 7000mAh(18650) 2S battery can be used for main power source.
* USB-PD 2Amps fast charging using USB-C port.
* One USB-A and display out for external display.
* USB-C formfactor Nurolink/docking port with UART, I2C.
* Additionally USB-A, USB-C, Audio out and Ethernet can be added with Add-on board.
* Full-Size 2280 M.2 SSD can be added with Add-on board.
* Full power off using OS and keyboard key.
* Read battery Power level and power off the device when battery is low with Add-on board.
* Improvements to casing to better fit out from the print compare to v4.

## Build instructions
* Order the PCB using this link [here](https://gitlab.com/mutantC/mutantc-v4/-/blob/master/parts_list)
* PCB Parts list can be found, [mainPCB](mainPCB/mainPCB.csv) and [displayPCB](displayPCB/displayPCB.csv)
* In addition to the parts, these parts needs to be printed [here](case/)
* Use this build guide to build it [here](bom/ibom.html)

## Supported add-ons
* [Add_on-apollo](https://gitlab.com/mutantcybernetics/add_on-apollo)

## Pics
<img src="pics/1.JPG" width="500">
<img src="pics/2.JPG" width="500">
<img src="pics/3.JPG" width="500">

## Feedback
We need your feedback to improve the mutantC.
Send us your feedback through GitLab [issues](https://gitlab.com/mutantcybernetics/mutantc-v5/-/issues).

## License
* Project is licensed under CC BY-NC-SA 4.0, See doc for more details - https://creativecommons.org/licenses/by-nc-sa/4.0/
* displayPCB is derived from Adafruit TFP401 HDMI breakout board. See the license below for that 
>Adafruit invests time and resources providing this open source design, please support Adafruit and open-source hardware by purchasing products from Adafruit!
>All text above must be included in any redistribution
>Designed by Limor Fried/Ladyada for Adafruit Industries. Creative Commons Attribution/Share-Alike, all text above must be included in any redistribution. See license.txt for additional information.
